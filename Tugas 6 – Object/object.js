function arrayToObject(arr) {
    // Code di sini
    var age = 0
    for(let i=0;i<arr.length;i++){
        var age = 0;
        var now = new Date();
        age += now.getFullYear() - arr[i][3];
        if(age<1 || arr[i][3]==undefined){age = "Invalid Birth Year"}
        var number = i+1;
        
        console.log(number+'. '+arr[i][0]+' '+arr[i][1]+': { fisrtname : "'+arr[i][0]+'", lastname : "'+arr[i][1]+'", gender : "'+arr[i][2]+'", age : '+age,'}');
    } 
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//soal 2

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var purchased = []
    var listPurchased = ["Sepatu Stacattu", "Baju Zoro", "Baju H&N", "Sweater Uniklooh", "Casing Handphone"]
    var price = [1500000, 500000, 250000, 175000, 50000]
   
    if(memberId == undefined && money == undefined || memberId == "") {
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        console.log("Mohon maaf, uang tidak cukup")
    } else {
        console.log("memberId:" + memberId)
        console.log("money:" + money)
        for(let i = 0; i<listPurchased.length; i++) {
            if (money >= price[i]) {
                money-=price[i]
                purchased.push(listPurchased[i])
    
            } 
        } 
        var list = purchased.toString()
    
        console.log("listPurchased: ")
        console.log("[" + list + "]")
        console.log("changeMoney: " + money)
    }
    
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  //soal 3
    function naikAngkot(arrPenumpang) {
        rute = ['A', 'B', 'C', 'D', 'E', 'F'];
        //your code here
        const biaya = 2000;
        var hasil = [];
        
        if (arrPenumpang.length === 0) {
          return arrPenumpang;
        }
        
        for (let i = 0; i < arrPenumpang.length; i++) {
          var penumpang = arrPenumpang[i];
          var objPenumpang = {};
          
          objPenumpang.penumpang = penumpang[0];
          objPenumpang.naikDari = penumpang[1];
          objPenumpang.tujuan = penumpang[2];
          objPenumpang.bayar = biaya * (rute.indexOf(objPenumpang.tujuan) - rute.indexOf(objPenumpang.naikDari));
          
          hasil.push(objPenumpang);
        }
        
        return hasil;
      }
       
      //TEST CASE
      console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
      // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
      //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
       
      console.log(naikAngkot([])); //[]
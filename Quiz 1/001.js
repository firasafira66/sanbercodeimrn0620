//soal a
function bandingkan(num1, num2) {
    if(num1 === num2) {
  return -1;
  }
  return num1 < num2;
  }

console.log(bandingkan(10, 15)); 
console.log(bandingkan(12, 12)); 
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121))
console.log(bandingkan(1)); 
console.log(bandingkan());
console.log(bandingkan("15", "18"))


//soal b
function balikString(string){
    let curStr = string
    let newStr = ""

    for (let i = string.length - 1; i >= 0; i--) {
        newStr = newStr + curStr[i];
    }
    return newStr
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

//soal c
function palindrome(string) {
    let string3 = string;
    let string4 = "";
  for (let i = string.length -1; i >= 0; i--) {
      string4 = string4 + string3[i];
    if (string4 == string3) {
      return true;
    } else if (string4 != string3) {
        return false;
    }
    return string
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
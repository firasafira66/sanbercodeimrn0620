console.log("Soal 1");
console.log("Looping Pertama");


var deret = 0;
while(deret < 20) {
  deret+=2; 
  console.log(deret + ' - I love coding ')
}

console.log("Looping Kedua");
var deret = 22
while(deret > 2) {
    deret-=2; 
    console.log(deret + ' - I will become a mobile developer ')
}

console.log('\n');
//for looping/
console.log("Soal 2");
for(var angka = 1;angka < 21; angka += 1) {
  if (angka%2==0) {
    console.log(angka + " - Berkualitas")
  }
  else if (angka%3==0) {
    console.log(angka + " - I love coding")
  }
  else {
    console.log(angka + " - Santai")
  }
}
console.log('\n');
//nomer 3
console.log("Soal 3");
var persegi = "########"
var no = 0
while(no < 4) {
  no++
  console.log(persegi)
}
console.log('\n');
//nomer 4
console.log("Soal 4");
var pager = "#"
var nomer = 0
while (nomer < 7) {
  pager += "#";
  nomer++;
  console.log(pager)
}
console.log('\n');
//nomer5
console.log("Soal 5");
var spasi='';
for (var i = 1; i <=8; i++) {
  if (i%2==0) {
   for (var j = 1; j <= 8; j++) {
    if (j%2==0) {
     spasi+='#';
    }else spasi+=' ';
   }
   spasi+='\n';
  }else{
   for (var j = 1; j <= 8; j++) {
    if (j%2==0) {
     spasi+=' ';
    }else spasi+='#';
   }
   spasi+='\n';
  }
}
console.log(spasi);
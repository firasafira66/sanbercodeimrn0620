//soal no 1
function range(startNum, finishNum) {
    var number = []
    if (startNum == " " || finishNum == "") {
        return -1
    } for(var i = startNum; i <=finishNum; i++) {
        number.push(i)
        return number
    } for(var i = startNum; i >= finishNum; i--) {
        number.push(i) 
        return number
    }
}

startNum = 1
finishNum = 10
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

//soal no 2
function rangeWithStep (startNum, finishNum, step) {
    var numbers = []
    if (startNum == " " || finishNum == "" || step == "") {
        return -1
    } for(startNum; startNum <= finishNum; startNum++) {
        numbers.push(startNum)
        return numbers
    } for(startNum; startNum >= finishNum; startNum--) {
        numbers.sort((a,b) => a-b)
        return numbers
    }
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
var nama = "john"
var peran = ""

if (nama == "") {
    console.log ("nama harus diisi!")
} 
else if (peran == "") {
    console.log ("halo " + nama + ", Pilih peranmu untuk memulai game!")
}
else if (peran == "Penyihir") {
    console.log ("Selamat datang di Dunia Werewolf," + nama)
    console.log ("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}
else if (peran == "Guard") {
    console.log ("Selamat datang di Dunia Werewolf," + nama)
    console.log ("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
else if (peran == "Werewolf") {
    console.log ("Selamat datang di Dunia Werewolf," + nama)
    console.log ("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
}


var hari = 10; 
var bulan = 9; 
var tahun = 2000;

switch(bulan){
    case 1: 
        bulan = "januari"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 2: 
        bulan = "februari"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 3: 
        bulan = "maret"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 4: 
        bulan = "april"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 5: 
        bulan = "mei"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 6: 
        bulan = "juni"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 7: 
        bulan = "juli"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 8: 
        bulan = "agustus"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 9: 
        bulan = "september"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 10: 
        bulan = "oktober"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 11: 
        bulan = "november"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    case 12: 
        bulan = "desember"
        console.log (hari + " " + bulan + " " + tahun)
        break;
    default:
        console.log ("masukkan data dengan valid!")
        break;
}